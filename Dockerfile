FROM openjdk:16-jdk-alpine

COPY target/*-single.jar /usr/app/app.jar

WORKDIR /usr/app

ENTRYPOINT ["java","-jar","app.jar"]
